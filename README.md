# Node Echo Server

A node.js server which receives JSON data from a POST request and echoes it back.

Mostly just here as a functional Node.js project template.
